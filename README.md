#	BugTracker Application
This project was undertaken as a project for a MSc in Software Engineering at Leeds Beckett University.

##	Aim and objectives
The overall aim of the project was to develop an application that would allow non-technical users to to file bug reports that are useful to technical users, with the following features

 - Tied in to repository hosting systems (GitHub) that a developer may use, but hiding this complexity from non-technical users. (Projects are imported from a users GitHub account, with a distinction between testers and developers)
 - Bug reports are produced via a form that aims to guide the information that has been given - to help ensure the report contains useful information.
 - A comments interface, enables testers and developers to discuss a bug report.
	
##	Gameplay/Images
#### Screen 1 - Login Screen
![Login Screen](https://gitlab.com/dan301290/BugTracker/raw/master/images/BugTracker1.png)

Shows the Login Screen of the application, which enables:

 - The user to login with their GitHub account - interface type determines the options the user will have and the projects that will appear with in the application.

#### Screen 2 - Projects Overview Screen
![Projects Overview Screen](https://gitlab.com/dan301290/BugTracker/raw/master/images/BugTracker2.png)

Shows the following:

 - A list of all the projects associated with the logged in user, the total number of open issues and further project details.
 - The option to create projects. (this option will also create a repository for the created project)
 -  The option to add users to selected projects. (users must have a GitHub account)
	
#### Screen 3 - Project Overview Screen
![Project Overview Screen](https://gitlab.com/dan301290/BugTracker/raw/master/images/BugTracker3.png)

Shows the following: 

 - Shows all issues/bugs that belong to a selected project along with the current status of each.
 - The full details of a bug report e.g. description, file, method and line numbers (bug reports can be edited and information such as line number, file and method can therefore be added by a developer after the initial report has been filed)
![Create Project Screen](https://gitlab.com/dan301290/BugTracker/raw/master/images/BugTracker4.png)
 - The ability to create and edit bug reports.
 - The ability to see and contribute to the conversation surrounding a bug report.
 - The ability to close an issue/bug report.
